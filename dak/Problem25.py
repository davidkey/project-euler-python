'''
Created on May 8, 2015

@author: David
'''

def fib(n):
    if(n == 1 or n == 2):
        return 1    
    return fib(n - 1) + fib(n - 2)


fibSeq = list((1,1))

for n in range(2, 5000):
    fibn = (fibSeq[n-1] + fibSeq[n-2])
    fibSeq.append(fibn)
    if(len(str(fibn)) >= 1000):
        print(len(fibSeq))
        print(fibn)
        break
    