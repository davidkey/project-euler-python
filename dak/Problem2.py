'''
Created on May 7, 2015

@author: David
'''

startNumber = 1
endNumber = 100

print("Unendlicher Fibonacci-Generator Rekursiv")
def fib(n):

    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


results=[fib(n) for n in range(1,34)]

result = 0
for i in results:
    if i % 2 == 0:
        result += i

print (result)
