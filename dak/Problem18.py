'''
Created on May 8, 2015

@author: David
'''
grid = [[75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[95, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[17, 47, 82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[18, 35, 87, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[20, 4, 82, 47, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[19, 1, 23, 75, 3, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[88, 2, 77, 73, 7, 63, 67, 0, 0, 0, 0, 0, 0, 0, 0],
[99, 65, 4, 28, 6, 16, 70, 92, 0, 0, 0, 0, 0, 0, 0],
[41, 41, 26, 56, 83, 40, 80, 70, 33, 0, 0, 0, 0, 0, 0],
[41, 48, 72, 33, 47, 32, 37, 16, 94, 29, 0, 0, 0, 0, 0],
[53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14, 0, 0, 0, 0],
[70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57, 0, 0, 0],
[91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48, 0, 0],
[63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31, 0],
[4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23]]

#grid = [[3,0,0,0],
#        [7,4,0,0],
#        [2,4,6,0],
#        [8,5,9,3]]

GRID_SIZE = len(grid) # 15

#init grid
gridCounter = []
for x in range(GRID_SIZE):
    new = []
    for y in range(GRID_SIZE):
        new.append(grid[x][y])
    gridCounter.append(new)

# calc pathes
for x in reversed(range(1, GRID_SIZE)):
    for y in reversed(range(GRID_SIZE)):
        currValue = gridCounter[x][y]
        if(y > 0 and grid[x-1][y-1] + currValue > gridCounter[x-1][y-1]):
            gridCounter[x-1][y-1] = grid[x-1][y-1] + currValue
        if(grid[x-1][y] + currValue > gridCounter[x-1][y]):
            gridCounter[x-1][y] = grid[x-1][y] + currValue
       
print(max(gridCounter[0]))