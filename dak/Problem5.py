'''
Created on May 7, 2015

@author: David
'''
from math import ceil

def isEvenlyDiv(num, setOfNumbers):
    for n in setOfNumbers:
        if(num % n != 0):
            return False
    return True
 

numbers = list(reversed(range(2,21)))
for n in numbers:
    for i in range(2, ceil(n/2) + 1):
        if(n % i == 0 and i in numbers):
            numbers.remove(i)

found = False
num = 20
while(not isEvenlyDiv(num, numbers)):
    num += 20
                
print (num)



    
def getPrimes(start, end):
    primes = []
    for n in range(start, end+1):
        if(isPrime(n)):
            primes.append(n)
    return primes
        

def isPrime(n):
    if(n <= 0):
        return False
    if(n == 1 or n == 2 or n == 3):
        return True
    if(n % 2 == 0):
        return False
    for i in range (3, ceil(n/2)):
        if(n % i == 0):
            return False
    return True
