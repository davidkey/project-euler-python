'''
Created on May 8, 2015

@author: David
'''

def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

MAX_PRIME = 1000

primes = sorted(list(sieve(MAX_PRIME)))

for n in reversed(primes):
    period = 1
    while pow(10, period, n) != 1:
        period += 1
    if n-1 == period: break
 
print ("longest recurring cycle for 1/d, d =", n)