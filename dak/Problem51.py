'''
Created on May 10, 2015

@author: David
'''

# sigh... http://www.mathblog.dk/project-euler-51-eight-prime-family/

MAX_NUM = 1000000


def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def eight_prime_family(prime, rd, primes):
    c=0
    for digit in '0123456789':
        n = int(prime.replace(rd, digit))
        if (n>100000 and n in primes):
            c=c+1
    return c==8


primes = set(sieve(MAX_NUM))

for prime in primes:
    if (prime>100000):
        s = str(prime)
        last_digit = s[5:6]
        if (s.count('0')==3 and eight_prime_family(s,'0', primes) \
         or s.count('1')==3 and last_digit != '1' and eight_prime_family(s,'1', primes) \
         or s.count('2')==3 and eight_prime_family(s,'2', primes)):
                print ("Project Euler 51 Solution: ",s)
