'''
Created on May 9, 2015

@author: David
'''

def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def isTruncatablePrime(n, primes):
    s = str(n)
    length = len(s)
    
    if(length <= 1):
        return False
    
    for i in range(length):
        if(int(s[i:]) not in primes):
            return False
        
    for i in reversed(range(1,length)):
        if(int(s[:i]) not in primes):
            return False
    return True

MAX_NUM = 1000000

primes = set(sieve(MAX_NUM))

results = []
for n in primes:
    if isTruncatablePrime(n, primes):
        results.append(n)

print(results)
print(len(results))
print("answer: " + str(sum(results)))