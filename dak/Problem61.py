'''
Created on May 12, 2015

@author: David
'''

# woah

def triangle(n):
    return int(n*(n+1)/2)

def square(n):
    return int(n*n)

def pentagonal(n):
    return int(n*(3*n-1)/2)

def hexagonal(n):
    return int(n*(2*n-1))

def heptagonal(n):
    return int(n*(5*n-3)/2)

def octagonal(n):
    return int(n*(3*n-2))

assert([triangle(n) for n in range(1, 6)] == [1, 3, 6, 10, 15])
assert([square(n) for n in range(1, 6)] == [1, 4, 9, 16, 25])
assert([pentagonal(n) for n in range(1, 6)] == [1, 5, 12, 22, 35])
assert([hexagonal(n) for n in range(1, 6)] == [1, 6, 15, 28, 45])
assert([heptagonal(n) for n in range(1, 6)] == [1, 7, 18, 34, 55])
assert([octagonal(n) for n in range(1, 6)] == [1, 8, 21, 40, 65])

triangles = [x for x in [triangle(x) for x in range(200)] if x >= 1000 and x < 10000]
squares = [x for x in [square(x) for x in range(200)] if x >= 1000 and x < 10000]
pentagonals = [x for x in [pentagonal(x) for x in range(200)] if x >= 1000 and x < 10000]
hexagonals = [x for x in [hexagonal(x) for x in range(200)] if x >= 1000 and x < 10000]
heptagonals = [x for x in [heptagonal(x) for x in range(200)] if x >= 1000 and x < 10000]
octagonals = [x for x in [octagonal(x) for x in range(200)] if x >= 1000 and x < 10000]

def fn(n):
    return (3,n*(n+1)/2), (4,n*n), (5,n*(3*n-1)/2), (6,n*(2*n-1)), (7,n*(5*n-3)/2), (8,n*(3*n-2))
 
def next(types, data):
    if len(types) == 6 and data[0] // 100 == data[-1] % 100:
        print (data, sum(data))
    else:
        for t, n in ds.get((types[-1], data[-1]), []):
            if t not in types:
                next(types+[t], data+[n])
 
p = []          # build a list of polygonal numbers with their type (type, pnum)
n = 19          # first n for octogonal number > 999
 
while n < 141:  # last n for triangle numbers < 10000
    for type, data in fn(n):
        if 1000 <= data <= 9999 and data % 100 > 9:
            p.append( (type, data) ) 
    n+=1
 
ds = {}         # build a dictionary of tuples
for t1, d1 in p:
    for t2, d2 in p:
        if t1 != t2 and d1 % 100 == d2 // 100:
            ds[t1, d1] = ds.get((t1, d1),[]) + [(t2, d2)] 
 
print ("Project Euler 61 Solution Set")
for type, data in ds: next([type], [data])
