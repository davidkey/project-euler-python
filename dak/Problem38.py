'''
Created on May 9, 2015

@author: David
'''
from operator import contains

#i ended up cheating - i had a working solution but it was slow as dirt... getting tired i think
# see https://codereview.stackexchange.com/questions/49321/project-euler-38-pandigital-multiples

def problem38():
    largest=int()      
    for i in range(9000,10000):
        temp=str(i)+str(i*2)
        if "0" not in temp:
            if len(set(temp))==9:
                if int(temp)>largest:
                    largest=int(temp)

    return largest 

print(str(problem38()))