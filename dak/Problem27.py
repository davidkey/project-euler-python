'''
Created on May 9, 2015

@author: David
'''

def sieve(n):
    "Return all primes <= n."
    np1 = n + 1
    s = list(range(np1)) # leave off `list()` in Python 2
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1): # use `xrange()` in Python 2
        if s[i]:
            # next line:  use `xrange()` in Python 2
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

primes = list(sieve(2000000))


def numPrimes(a, b, primes):
    for n in range (0,9999):
        res = n**2 + (a*n) + b;
        if(res in primes):
            continue
        return n
    

primes = set(sieve(2000000))
#print(numPrimes(10, 7, primes))


largest = (0, (0,0))
for a in range(-1000, 1000):
    for b in range(-1000,1000):
        res = numPrimes(a,b,primes)
        if(res > largest[0]):
            largest = (max((largest[0], res)), (a,b)) 
    
print(largest)
print ( largest[1][0] * largest[1][1] ) #answer

#actually did this without cheating - random brute force attempt worked first try! so proud...