'''
Created on May 9, 2015

@author: David
'''

def isPalindrome(s):
    s = str(s)
    return s == s[::-1]

results = []
for n in range(1000000):
    if (isPalindrome(n) and isPalindrome(str(bin(n))[2:])):
        results.append(n)
        
print(sum(results))