'''
Created on May 10, 2015

@author: David
'''
import math
from itertools import permutations


def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def is_prime(n):
    n = int(n)
    if n % 2 == 0 and n > 2: 
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

primes = list(sieve(10000))
set_size = 5

def make_chain(chain):
    if len(chain) == set_size:
        return chain 
    for p in primes:
        if p > chain[-1] and all_prime(chain+[p]):
            new_chain = make_chain(chain+[p])
            if new_chain:
                return new_chain
    return False  
        
def all_prime(chain):
    return all(is_prime(str(p[0]) + str(p[1])) for p in permutations(chain, 2))

chain = 0
while not chain:
    chain = make_chain([primes.pop(0)])

print ("Project Euler 60 Solution =", sum(map(int, chain)), chain)