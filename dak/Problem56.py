'''
Created on May 12, 2015

@author: David
'''

MAX = 100


maxDigitCount = 0
for a in range(1,MAX):
    for b in range(1, MAX):
        dc = sum(int(x) for x in list(str(a**b)))
        if(dc > maxDigitCount):
            maxDigitCount = dc
print ('answer: ', maxDigitCount)