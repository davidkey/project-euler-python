'''
Created on May 9, 2015

@author: David
'''
from itertools import product

MAX_NUM = 1000000

def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def cycles(s):
    s = str(s)
    cycles = set()
    for n in range(len(s)):
        cycles.add(int(s[n:] + s[:n]))
    return cycles

####

primes = set(sieve(MAX_NUM)) # get prime numbers below MAX_NUM
possibilities = [2,3,5,7] # init primes below 10 by hand

for n in range(2, len(str(MAX_NUM))):
    possibilities += [int(x) for x in [''.join(x) for x in product('13579', repeat=n)]] #any cycle above 10 will only contain digits '13579'
    
possibilities = set(possibilities)
candidates = [x for x in primes if x in possibilities]
results = [c for c in candidates if cycles(c).issubset(primes)]

print ("results: " + str(sorted(list(results))))
print ("answer: " + str(len(results)))


