'''
Created on May 8, 2015

@author: David
'''

MONTH_DAYS = (31,28,31,30,31,30,31,31,30,31,30,31)

def isLeapYear(y):
    if y % 100 == 0:
        return y % 400 == 0
    return y % 4 == 0

def getDaysInMonth(year, month):
    return (MONTH_DAYS[month-1]) + (1 if (month == 2 and isLeapYear(year)) else 0)

elapsedDays = 0
sundayCount = 0
for y in range(1900, 2001):
    for m in range(1, 13):
        if(y > 1900 and elapsedDays % 7 - 6 == 0):
            sundayCount += 1
        elapsedDays += getDaysInMonth(y, m)
print(sundayCount)
