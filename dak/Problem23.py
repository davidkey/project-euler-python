'''
Created on May 7, 2015

@author: David
'''
from math import sqrt, floor

def canBeSummedBy(n, l):
    for i in l:
        if(i >= n):
            return False
        if ((n - i) in l):
                return True
    return False

def isAbundant(i):
    factors = []
    for n in range(1, floor(sqrt(i) + 1)):
        if(i % n == 0):
            factors.append(n)
            if(n > 1):
                factors.append(int(i / n))
    return sum(set(factors)) > i

abuntantNumbers = []
for n in range(1, 28124):
    if(isAbundant(n)):
        abuntantNumbers.append(n)        
abuntantNumbers = set(abuntantNumbers)

results = []
for n in range(1, 28124):
    if(not canBeSummedBy(n, abuntantNumbers)):
        results.append(n)
        
print (sum(results))
