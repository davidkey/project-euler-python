'''
Created on May 9, 2015

@author: David
'''
from _functools import reduce

def pentagonal(r): # get set of first r pentagonal numbers
    results = set()
    for n in range (1,r+1):
        results.add(int(n*(3*n-1)/2))
    return results

def findPair(pent):
    for p in pent:
        for x in pent:
            if(x == p):
                continue
            if(x+p in pent and abs(x-p) in pent):
                return [x, p]

print (reduce(lambda x,y: abs(x-y), findPair(pentagonal(10**4))))