'''
Created on May 12, 2015

@author: David
'''

def decryptXor(chars, key):
    result = []
    keyLen = len(key)
    count = 0
    for c in chars:
        result.append(c ^ key[count % keyLen])
        count += 1
    
    return result

def convertToAscii(charOrds):
    return [chr(x) for x in charOrds]

def couldBeEnglish(i, englishWords):
    matchCount = 0
    unmatchCount = 0
    for word in i:
        if word in englishWords:
            matchCount += 1
        else:
            unmatchCount += 1
        if(matchCount > 40):
            return True
        if(unmatchCount > 200):
            return False
    return False

def decrypted(chars, englishWords):
    for x in range(ord('a'), ord('z') + 1):
        for y in range(ord('a'), ord('z') + 1):
            for z in range(ord('a'), ord('z') + 1):
                decrypted = decryptXor(chars, [x,y,z])
                results = ''.join(convertToAscii(decrypted)).lower()
                resultWords = results.split(' ')
                if len(resultWords) > 0:
                    if couldBeEnglish(resultWords, englishWords):
                        #print ('key:', [x,y,z], 'result:', results)
                        return decrypted;

with open("C:\\Temp\\p059_cipher.txt") as f:
    chars = f.read().split(',')
    chars = [int(x) for x in chars]

with open("C:\\Temp\\google-10000-english.txt") as f:
    englishWords = set(f.read().split('\n'))


result = decrypted(chars, englishWords)
print (sum(result))
            

            
