'''
Created on May 12, 2015

@author: David
'''


L, n, d, c = 1000, 3, 2, 0

for x in range(2, L):
    n, d = n + d*2, n + d
    if len(str(n)) > len(str(d)): c += 1

print ("Project Euler 57 Solution =", c)