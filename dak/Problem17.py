'''
Created on May 7, 2015

@author: David
'''

import inflect

MAX_NUMBER = 1000

infl = inflect.engine()

nlc = 0
for n in range(1, MAX_NUMBER + 1):
    nlc += len(infl.number_to_words(n).replace(" ", "").replace("-", ""))
    
print (nlc)