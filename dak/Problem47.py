'''
Created on May 9, 2015

@author: David
'''

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

lastFound = 0
consec = 1
def findDistictPrimeFactorSeq(seqLength):
    results = []
    lastFound = 0
    consec = 1
    for n in range(1, 999999999):
        uniqFactors = set(prime_factors(n))
        if(len(uniqFactors) == seqLength):
            if(lastFound == n-1):
                consec += 1
            else:
                consec = 1
            lastFound = n
            
            if(consec == seqLength):
                for i in reversed(range(seqLength)):
                    results.append(str(n-i))
                return results

res = findDistictPrimeFactorSeq(4)
print (res)
