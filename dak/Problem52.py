'''
Created on May 10, 2015

@author: David
'''


def isPermutedMultiple(n):
    l = sorted(list(str(n)))
    for x in range(2, 7):
        if(not sorted(list(str(x*n))) == l):
            return False
    return True


for n in range(1, 10**6):
    if(isPermutedMultiple(n)):
        print ("match found: {0}".format(n))
