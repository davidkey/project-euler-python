'''
Created on May 10, 2015

@author: David
'''

def isLychrel(n):
    for x in range(50):
        n = n + int(str(n)[::-1])
        if(str(n) == str(n)[::-1]):
            return False
    return True

assert(not isLychrel(47))
assert(not isLychrel(349))
assert(isLychrel(196))

count = 0
for n in range(10000):
    if(isLychrel(n)):
        count += 1
        
print ('answer: ', count)