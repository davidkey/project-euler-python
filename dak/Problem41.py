'''
Created on May 9, 2015

@author: David
'''

def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def isPandigital(n):
    s = set(int(x) for x in list(str(n)))
    for n in range(1, len(str(n))+1):
        if n not in s:
            return False
    return True


primes = set(sieve(10000))
pandigitalPrimes = [x for x in primes if isPandigital(x)]
print (max(pandigitalPrimes))