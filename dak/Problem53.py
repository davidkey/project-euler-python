'''
Created on May 10, 2015

@author: David
'''
from math import factorial

def getC(n, r):
    return factorial(n) / (factorial(r) * factorial(n-r))   

count = 0
for n in range(1, 101):
    for r in range(1, n+1):
        if(getC(n, r) > 10**6):
            count += 1
    
print(count)