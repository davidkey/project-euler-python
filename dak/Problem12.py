'''
Created on May 7, 2015

@author: David
'''
from operator import add
from _functools import reduce

import queue
import threading

def factors(n):    
    return set(reduce(list.__add__, 
                ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))

def triagleNums(n):
    nums = []
    count = 1
    while(len(nums) < n):
        nums.append(reduce(add, [int(y) for y in range(1, count+1)]))
        count += 1
    return nums

def checkSumOfFactors(q, f):
    for n in f:
        if(int(len(factors(n)) > 500)):
            print (n)
#            q.put(n)
            q.put_nowait(n)



tn = triagleNums(15000)

#for n in tn:
 #   if(len(factors(n)) >= 500):
  #      print (n)
        


q = queue.Queue

sliceSize = int(len(tn) / 2)
slice1 = tn[0:sliceSize]
slice2 = tn[sliceSize:]

slices = [slice1, slice2]

for s in slices:
    t = threading.Thread(target=checkSumOfFactors(q, s), args = (q, s))
    t.daemon = True
    t.start()

s = q.get()
print ("s = " + s)
