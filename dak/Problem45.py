'''
Created on May 10, 2015

@author: David
'''

def triangleNum(r):
    results = set()
    for n in range(1, r+1):
        results.add(int( n*(n+1)/2 ))
    return results

def pentagonalNum(r):
    results = set()
    for n in range(1, r+1):
        results.add(int( n*(3*n-1)/2 ))
    return results

def hexagonalNum(r):
    results = set()
    for n in range(1, r+1):
        results.add(int( n*(2*n-1) ))
    return results

RESULT_SIZE = 60000 # this was enough to get 3 numbers

#tn = triangleNum(RESULT_SIZE) # all pent numbers are by default triangle numbers
pn = pentagonalNum(RESULT_SIZE)
hn = hexagonalNum(RESULT_SIZE)

#print([x for x in tn if x in pn and x in hn])
print([x for x in pn if x in hn])