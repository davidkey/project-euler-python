'''
Created on May 12, 2015

@author: David
'''

from _collections import defaultdict

d = defaultdict(list)

for c in [x**3 for x in range(1, 10000)]:
    d[tuple(sorted(list(str(c))))].append(c)

print(min([min(d[x]) for x in d if len(d[x]) == 5]))