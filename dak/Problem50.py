'''
Created on May 10, 2015

@author: David
'''

MAX_NUM = 1000000


def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def consecPrimeSum(startingMax, n, primes, primeSumCandidates):
    candidates = sorted([x for x in primeSumCandidates if x >= n])
    maxn = 0
    maxList = []
    for x in range(startingMax, len(candidates)):
        sumc = sum(candidates[0:x])
        if(sumc >= MAX_NUM):
            return [maxn, sum(maxList), maxList]
        if(sum(candidates[0:x]) in primes):
            maxn = x
            maxList = candidates[0:x]
    return [maxn, sum(maxList), maxList]

primes = set(sieve(MAX_NUM-1))
primeSumCandidates = [x for x in primes if x < 5000] # terms above 5000 sum to > 1 million
orderedPrimes = sorted(list(primes))

maxn = [0, 0, 0]
for p in primes:
    res = consecPrimeSum(maxn[0], p, primes, primeSumCandidates)
    if(res[0] > maxn[0]):
        maxn = res
        print(maxn)
print ("answer: " + str(maxn))

#for p in orderedPrimes:
    

print ('done')
