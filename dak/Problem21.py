'''
Created on May 8, 2015

@author: David
'''
from math import floor

def factors(n):
    factors = []
    for x in range(1,floor(n/2)+1):
        if(n % x == 0):
            factors.append(x)
    return factors
        
amicableNumSum = 0
for n in range(10000):
    sf = sum(factors(n))
    if(sf != n and sf > n and sum(factors(sf)) == n):
        amicableNumSum += (n + sf)

print(amicableNumSum)