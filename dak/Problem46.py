'''
Created on May 10, 2015

@author: David
'''

def twiceSquares(r): # up to r
    result = set()
    for x in range(1, r+1):
        result.add(2 * x**2)
    return result


def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

MAX_NUM = 10000

primes = set(sieve(MAX_NUM))
ts = twiceSquares(MAX_NUM)

res = set()
for p in primes:
    for s in ts:
        res.add(p + s)

ans = [x for x in range(3, max(primes), 2) if x not in res and x not in primes]        
print(min(list(ans)))
