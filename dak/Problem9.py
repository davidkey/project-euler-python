'''
Created on May 7, 2015

@author: David
'''
from _functools import reduce

for n in range(3, 500):
    for x in range(1, n):
        for y in range(1, x):
            if(x**2 + y**2 == n**2):
                if(sum([x, y, n]) == 1000):
                    print (reduce(lambda x, y: x*y, [x, y, n]))
