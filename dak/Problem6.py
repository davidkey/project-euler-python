'''
Created on May 7, 2015

@author: David
'''

def sumOfSquare(r):
    return sum([x**2 for x in r])

def squareOfSum(r):
    return sum(r)**2
        
def problem6(r):
    return sum(r)** 2- sum([x** 2 for x in r])
        
r = range(1, 101)        
sumSquare = sumOfSquare(r)
squareSum = squareOfSum(r)

print(sumSquare)
print(squareSum)
print(squareSum-sumSquare)
