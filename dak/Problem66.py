'''
Created on May 10, 2015

@author: David
'''
from math import sqrt

def sqDict(r): #square root dict <= r
    d = dict()
    for n in range(1, r+1):
        sq = sqrt(n)
        if sq == int(sq):
            d[n] = sq
    return d

def qd(d):
    for x in range(1, 100000):
        for y in reversed(range(x,100000)):
            if(x**2 - (d*(y**2)) == 1):
                return x

def qd2(d, sq):
    for y in range(1, 1000000):
        xSquared = (d * y**2)+1
        if xSquared in sq:
            return sq[xSquared]
'''        
        x = sqrt((d * y**2)+1)
        if(x < y):
            return
        print (x)
        if(int(x) == x):
            return int(x)
'''

sq = sqDict(10000000)
#print (qd2(13, sq))   
    
maxResult = 0        
maxD = 0
for x in range(2, 1001):
    print ("processing ", x)
    res = qd2(x, sq)
    if(res and res > maxResult):
        maxResult = res
        maxD = x

print(maxD, maxResult)