'''
Created on May 9, 2015

@author: David
'''

cents = [x for x in range(1, 201)]
twoCents = [x for x in range(2, 201, 2)]

print (cents)
print (twoCents)

AMT = 200 # 2 pounds
#AMT = 50 # 2 pounds
count = 0
for twoPound in range(0, int(AMT/200)+1):
    for onePound in range(0, int(AMT/100)+1):
        for fiftyP in range(0, int(AMT/50)+1):
            for twentyP in range(0, int(AMT/20)+1):
                for tenP in range(0, int(AMT/10) + 1):
                    for fiveP in range(0, int(AMT/5)+1):
                        for twoP in range(0, int(AMT/2)+1):
                            for oneP in range(0, AMT+1):
                                s = twoPound * 200 + onePound * 100 + fiftyP * 50 + twentyP * 20 + tenP * 10 + fiveP * 5 + twoP * 2 + oneP * 1
                                if(s > AMT):
                                    break
                                if(s == AMT):
                                    print ('found match - ' + str([twoPound, onePound, fiftyP, twentyP, tenP, fiveP, twoP, oneP]))
                                    count += 1

print('matches found: ' + str(count))
