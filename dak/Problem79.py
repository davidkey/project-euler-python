'''
Created on May 13, 2015

@author: David
'''

import re
import sys
from itertools import permutations

priorAttempts = [
319,
680,
180,
690,
129,
620,
762,
689,
762,
318,
368,
710,
720,
710,
629,
168,
160,
689,
716,
731,
736,
729,
316,
729,
729,
710,
769,
290,
719,
680,
318,
389,
162,
289,
162,
718,
729,
319,
790,
680,
890,
362,
319,
760,
316,
729,
380,
319,
728,
716]


def attemptAll(successfulAttempts, n):
    n = str(n)
    for sa in successfulAttempts:
        s = str(sa)
        if(not re.search(s[0] + '.*' +  s[1] + '.*' + s[2] + '.*', n)):
            return False
    return True


uniq = set(list(''.join(str(x) for x in priorAttempts)))
candidates = [int(y) for y in [''.join(x) for x in list(permutations(list(uniq), len(uniq)))]]

for c in candidates:
    if(attemptAll(priorAttempts, c)): # attempts is a list containing file given in problem description
        print (c)
        sys.exit()






