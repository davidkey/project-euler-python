'''
Created on May 9, 2015

@author: David
'''
from itertools import product, combinations, permutations

def pandigitals(mask, size):
    pd = set()
    for x in permutations(mask, size):
        val = int(''.join(x))
        if(val - (10**(MAX_SIZE-1)) > 0):
            pd.add(val)
    return pd;

def hasSubstringProperty(num, primes):
    pieces = str(num)
    for i in range(1,8):
        if (int(pieces[i:i+3]) % primes[i] != 0):
            return False
    return True
        
 
MAX_SIZE = 10
PRIMES_LOOKUP=[0,2,3,5,7,11,13,17]

pandigitals = pandigitals('0123456789', MAX_SIZE)

assert(1406357289 in set(pandigitals))
assert(hasSubstringProperty(1406357289, PRIMES_LOOKUP))

results = []
for n in pandigitals:
    if(hasSubstringProperty(n, PRIMES_LOOKUP)):
        results.append(n)

print (sum(results))
