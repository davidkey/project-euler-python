'''
Created on May 9, 2015

@author: David
'''

def triangles(r):
    t = set()
    for n in range(1, r):
        t.add( int( (1/2) * n * (n+1) ) )
    return t

def wordValue(w):
    pieces = list(str(w))
    score = 0
    for p in pieces:
        score += ord(p) - 64 #(A-1)
    return score


with open("C:\\Temp\\p042_words.txt") as f:
    names = f.read().replace('"', '').split(",")

t = triangles(100)
triWords = [w for w in names if wordValue(w) in t]
print (len(triWords))
