'''
Created on May 9, 2015

@author: David
'''
from math import ceil

def hasPandigitalProduct(n):
    for m1 in range(1, ceil(n/2)):
        if(n % m1 == 0):
            m2 = int(n / m1)
            res = ''.join(str(x) for x in (n,m1,m2))
            res = ''.join(sorted(list(res)))
            if(len(res) == 9 and res == '123456789'):
                return True
    return False

res = []
for n in range(4000, 8000): # common sense (i guessed...)
    if(hasPandigitalProduct(n)):
        res.append(n)

print (sum(res))