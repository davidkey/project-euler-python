'''
Created on May 10, 2015

@author: David
'''
from itertools import permutations
from _functools import reduce

def sieve(n): #    return all primes <= n
    np1 = n + 1
    s = list(range(np1))
    s[1] = 0
    sqrtn = int(round(n**0.5))
    for i in range(2, sqrtn + 1):
        if s[i]:
            s[i*i: np1: i] = [0] * len(range(i*i, np1, i))
    return filter(None, s)

def primePermutations(p, primes):
    perms = set([x for x in [int(y) for y in [''.join(x) for x in list(permutations(str(p), len(str(p))))]] if x in primes and x > p])
    for n in perms:
        if  n + (n-p) in perms:
            return ((p, n, n + (n-p))) 

def primePermutations2(p, primes):
    perms = set([x for x in [int(y) for y in [''.join(x) for x in list(permutations(str(p), len(str(p))))]] if x in primes and x > p])
    return [[p, n, n+(n-p)] for n in perms if n + (n-p) in perms]

primes = list(sieve(9999))
primes = set([x for x in primes if x > 1487])

for p in sorted(list(primes)):
    res = primePermutations(p, primes)
    if(res):
        print (reduce(lambda x,y: x + y, (str(x) for x in res)))
