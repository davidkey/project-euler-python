'''
Created on May 9, 2015

@author: David
'''

results = set()
for n in range(2, 1000000):
    fifthPowers = map(lambda x: x**5, ([int(i) for i in str(n)]))
    if (sum(fifthPowers) == n):
        results.add(n)
        
        
print(list(results))
print(sum(results))
