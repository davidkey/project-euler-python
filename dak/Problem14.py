'''
Created on May 7, 2015

@author: David
'''

MAX_NUM = 1000000
CACHE = dict()

def collatz(n):
    if n in CACHE: 
        return CACHE[n]
    seq = [int(n)]
    if(n == 1):
        return seq
    if(n % 2 == 0):
        seq.extend(collatz(n/2))
    else:
        seq.extend(collatz(3*n + 1))
    CACHE[n] = seq
    return seq
    

biggestSeq = (0, 0, 0)
for n in range(1, MAX_NUM):
    res = collatz(n)
    if(len(res) > biggestSeq[1]):
        biggestSeq = (n, len(res), res)
        
print(biggestSeq)