'''
Created on May 9, 2015

@author: David
'''
from _collections import defaultdict
from math import sqrt

def intRightTrianglesNaive(r): # up to r ## this was the method that answered the question for me, but i improved it below
    results = defaultdict(list)
    for a in range(1, r+1):
        for b in range(1, r+1):
            c = sqrt(a**2 + b**2) #a**2 + b**2 = c**2
            if c == int(c):
                tup = sorted([a,b,int(c)])
                if(tup not in results[int(sum(tup))]):
                    results[int(sum(tup))].append(tup)
    return results

def intRightTriangles(p): # up to max perim of p
    results = defaultdict(list)
    for a in range(1, int(p/4)):
        for b in range(a+1, int((p-a)/2)):
            c = sqrt(a**2 + b**2) #a**2 + b**2 = c**2  ## c = (a**2+b**2)**0.5 #<- ALSO WORKS
            if c == int(c):
                tup = sorted([a,b,int(c)])
                results[int(sum(tup))].append(tup)
    return results



irt = intRightTriangles(1000)

maxResults = 0
maxNum = 0
for p in irt:
    if len(irt[p]) > maxResults:
        maxNum = p
        maxResults = len(irt[p])
        
print ([maxNum, maxResults])