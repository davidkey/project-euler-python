'''
Created on May 11, 2015

@author: David
'''

print("My solution: ")
print('\n'.join(list(map(lambda x: "FizzBuzz" if x%3==0 and x%5==0 else "Fizz" if x%3==0 else "Buzz" if x%5==0 else str(x), range(1,101)))))

print("Other (better?) solution: ")
print('\n'.join(['Fizz'*(not i%3) + 'Buzz'*(not i%5) or str(i) for i in range(1, 101)]))