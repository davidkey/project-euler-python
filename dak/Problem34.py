'''
Created on May 9, 2015

@author: David
'''

def factorial(n):
    if(n == 0):
        return 1
    if(n == 1):
        return 1
    return n * factorial(n-1)

factorialLookup = [factorial(n) for n in range(10)]

res = []
for n in range(3,55555):
    parts = list((int(x) for x in list(str(n))))
    if (sum(factorialLookup[x] for x in parts) == n):
        res.append(n)

print (sum(res))