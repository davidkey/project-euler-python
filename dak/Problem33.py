'''
Created on May 9, 2015

@author: David
'''
from fractions import gcd
from math import floor

def reduce(n, d):
    g = gcd(n,d)
    if(g > 1):
        return (n/g) / (d/g)
    return n/d

np = 0
dp = 0
for n in range(10, 99):
    for d in range(n+1, 100):
        if(floor(n/10) == d%10 or floor(d/10) == n%10):
            if(d%10 != 0 and reduce(n, d) == reduce(floor(n/10), d%10)):
                np = np*n if np > 0 else n
                dp = dp*d if dp > 0 else d    
                print (n, d)

print(int(dp/gcd(np, dp)))