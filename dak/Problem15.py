'''
Created on May 8, 2015

@author: David
'''
# see http://www.mathblog.dk/project-euler-15/
GRID_SIZE = 20
grid = [[1 for x in range(GRID_SIZE+1)] for x in range(GRID_SIZE+1)]

for x in list(reversed(range(GRID_SIZE))):
    for y in list(reversed(range(GRID_SIZE))):
        grid[x][y] = grid[x+1][y] + grid[x][y+1]
        
print (grid[0][0])