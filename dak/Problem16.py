'''
Created on May 8, 2015

@author: David
'''

def sumDigits(n):
    s = 0
    parts = list(str(n))
    parts = map(int, parts)
    for p in parts:
        s += p
    return s

num = 2**1000

print(sumDigits(num))

#print(sum(reduce(add, [range(2**15)])))